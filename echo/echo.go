// Echo Protocol RFC 862
package main

import (
	"flag"
	"io"
	"log"
	"net"
	"strconv"
)

var port = flag.Int("port", 7, "port to listen for TCP connections")

func main() {
	flag.Parse()
	p := strconv.Itoa(*port)
	l, err := net.Listen("tcp", ":"+p)
	if err != nil {
		log.Fatalln("network error: " + err.Error())
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println("connection error: " + err.Error())
			continue
		}
		go echo(conn)
	}
}

func echo(c net.Conn) {
	_, err := io.Copy(c, c)
	if err != nil && err != io.EOF {
		log.Println("copy error: " + err.Error())
	}
}
