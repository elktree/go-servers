# go-servers

A miscellaneous group of network servers.

As the license says, **this software lacks any FITNESS FOR A PARTICULAR PURPOSE.**

| server    |tcp    |udp    |RFC    | Name |
| ---       |---    |---    |---    |---   |
| echo      |7      |       |682    |Echo Protocol          |
| daytime   |13     |13     |867    |Daytime Protocol       |
| quote     |17     |17     |865    |Quote of the Day Protocol       |
| message   |18     |18     |1312   |Message Send Protocol 2    |
| ~~ftp~~   |20/21  |       |959    |File Transfer Protocol     |
| telnet    |23     |       |854    |Telnet Protocol            |

dict https://tools.ietf.org/html/rfc2229#section-2.1
