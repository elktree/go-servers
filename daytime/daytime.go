// Daytime Protocol RFC 867
package main

import (
	"flag"
	"log"
	"net"
	"strconv"
	"time"
)

var port = flag.Int("port", 13, "port to listen for TCP and UDP connections")

func main() {
	flag.Parse()
	p := strconv.Itoa(*port)
	address := ":" + p

	tcp, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalln("tcp network error: " + err.Error())
	}
	defer tcp.Close()

	udp, err := net.ListenPacket("udp", address)
	if err != nil {
		log.Fatalln("udp network error: " + err.Error())
	}
	defer udp.Close()

	go listen(tcp)
	// udp loop
	buff := make([]byte, 1500) // ignore input, reuse buffer
	for {
		_, addr, err := udp.ReadFrom(buff)
		if err != nil {
			log.Println("udp connection error: " + err.Error())
			continue
		}
		dt := daytime()
		//addr, _ = net.ResolveUDPAddr("udp", "127.0.0.1:2000")
		n, err := udp.WriteTo(dt, addr)
		if err != nil || n != len(dt) {
			log.Println("udp write error: " + err.Error())
		}
		//log.Println(string(buff) + string(dt) + addr.String())
	}
}

func listen(l net.Listener) {
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println("tcp connection error: " + err.Error())
			continue
		}
		dt := daytime()
		n, err := conn.Write(dt)
		if err != nil || n != len(dt) {
			log.Println("tcp write error: " + err.Error())
		}
		conn.Close()
	}
}

func daytime() []byte {
	// Mon Jan 2 15:04:05 MST 2006
	dt := time.Now().Format("Monday, January 2, 2006 15:04:05-MST")
	bytes := append([]byte(dt), byte(0)) // ascii string with null terminal
	return bytes
}
