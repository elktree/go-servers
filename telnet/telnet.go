package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strconv"

	"github.com/containerd/console"
)

func main() {
	defer fmt.Println("exit telnet")

	// Data written to the slave is presented on the master file descriptor as
	// input.  Data written to the master is presented to the slave as input.

	// make a new console/pseudo-terminal pair
	con, ptspath, err := console.NewPty()
	if err != nil {
		panic("console error:" + err.Error())
	}
	pts, err := os.OpenFile(ptspath, os.O_RDWR, 0666)
	if err != nil {
		panic("pseudo-terminal error:" + err.Error())
	}
	defer pts.Close()

	// console settings
	con.SetRaw()
	con.DisableEcho()
	console.ClearONLCR(con.Fd())

	console.ClearONLCR(pts.Fd())

	// // read from con/pts and output to Stdout
	// go func() {
	// 	fromPts := bufio.NewScanner(con)
	// 	fromPts.Split(bufio.ScanBytes)
	// 	for fromPts.Scan() {
	// 		b := fromPts.Bytes()
	// 		fmt.Println("console: " + string(b))
	// 		os.Stdout.Write(b)
	// 	}
	// }()

	// read Stdin and send to con/pts
	go func() {
		for {
			io.Copy(os.Stdout, con)
		}
		// s := bufio.NewScanner(con)
		// s.Split(bufio.ScanBytes)
		// for s.Scan() {
		// 	b := s.Bytes()
		// 	fmt.Println("ptm: " + string(b))
		// 	//pts.Write(b)
		// }
	}()

	// out, _ := exec.Command("ls", "-lah").CombinedOutput()
	// os.Stdout.Write(out)

	// con.Write([]byte("ls -la\n"))
	// pts.Write([]byte("ls -lh\n"))
	// b := make([]byte, 1000)
	// n, _ := pts.Read(b)
	// os.Stdout.Write(b[:n])

	args := os.Args
	fmt.Println(args)
	cmd := exec.Command(os.Args[1], os.Args[2:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = pts
	cmd.Stderr = pts
	cmd.Start()

	// go func() {
	// 	for {
	// 		io.Copy(cmd.Stdin, os.Stdin)
	// 	}
	// }()

	// go func() {
	// 	fromPts := bufio.NewScanner(cmd.Stderr)
	// 	for fromPts.Scan() {
	// 		fmt.Println("cmd.Stderr: " + fromPts.Text())
	// 		//os.Stdout.WriteString(fromPts.Text())
	// 	}
	// }()

	for {
	}
	cmd.Wait()
}

func main3() {
	// con, ptspath, err := console.NewPty()
	// if err != nil {
	// 	panic(err.Error())
	// }
	// pts, err := os.Open(ptspath)
	// if err != nil {
	// 	panic(err.Error())
	// }

	// c := os.Args[1]
	// args := make([]string, 0)
	// io.Copy(args, os.Args)
	args := os.Args
	fmt.Println(args)
	cmd := exec.Command(os.Args[1], os.Args[2:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}

func main2() {

	//current := console.Current()
	var current console.Console
	current, s, _ := console.NewPty()
	fmt.Println(s + "\n" + current.Name())
	defer current.Reset()

	f, _ := os.OpenFile(s, os.O_RDWR, 0)
	//f.WriteString("ls")
	go func() {

		// scan2 := bufio.NewScanner(f)
		// for scan2.Scan() {
		// 	fmt.Println("read: " + scan2.Text())
		// 	//current.Write([]byte(scan.Text()))
		// }

		var p []byte = make([]byte, 1000)
		for {

			n, err := current.Read(p)
			if n < 1 || err != nil {
				fmt.Println(strconv.Itoa(n) + err.Error())
				continue
			}
			fmt.Println("tty: " + string(p[:n]))
		}
	}()

	current.DisableEcho()
	//current.Write([]byte("bash -bash\n"))

	// var p []byte = make([]byte, 1000)
	// n, _ := current.Read(p)
	// fmt.Println(string(p[:n]))

	//if err := current.SetRaw(); err != nil {}
	//current.Write([]byte("echo hello\n"))

	// ws, _ := current.Size()
	// current.Resize(ws)

	go func() {
		cmd := exec.Command("/bin/echo", "hello")
		// wc, _ := cmd.StdinPipe()
		//rc, _ := cmd.StdoutPipe()
		//pr, pw := io.Pipe()

		//cmd.Stdin =
		//cmd.Stdout = bufio.NewWriter(f)

		// scan := bufio.NewWriter(cmd.Stdout)
		// go func() {
		// 	for scan. {
		// 		text := scan.Text()
		// 		fmt.Println("cmd.Stdout: " + text)

		// 		f.WriteString(text)
		// 	}
		// }()

		// var out bytes.Buffer
		// cmd.Stdout = &out

		r := bufio.NewReader(f)
		cmd.Stdin = r
		w := bufio.NewWriter(f)
		cmd.Stdout = w

		err := cmd.Run()
		if err != nil {
			fmt.Println("cmd error: " + err.Error())
		}
		//fmt.Printf("in all caps: %q\n", cmd.String())
		fmt.Println("cmd.Run() done")
	}()

	scan := bufio.NewScanner(os.Stdin)
	for scan.Scan() {
		text := scan.Text()
		fmt.Println("writing: " + text)
		current.Write([]byte(text))
		//f.WriteString(scan.Text())
	}

}

// //current := console.Current()
// var current console.Console
// current, _, _ = console.NewPty()
// defer current.Reset()

// current.DisableEcho()
// var p []byte = make([]byte, 100)
// current.Read(p)
// current.Write([]byte("bash \n" + string(p)))
// if err := current.SetRaw(); err != nil {
// }
// current.Write([]byte("echo hello\n"))

// ws, _ := current.Size()
// current.Resize(ws)
