// Quote of the Day Protocol RFC 865
package main

import (
	"flag"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"strconv"
	"strings"
)

var quotefile = flag.String("quotes", "quotes.txt",
	"file containing quotes separated by blank lines")
var port = flag.Int("port", 17, "port to listen for TCP and UDP connections")

var quotes []string = make([]string, 0)

func main() {
	flag.Parse()
	address := ":" + strconv.Itoa(*port)
	rand.Seed(42)
	readquotes(*quotefile)

	// open tcp
	tcp, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalln("tcp network error: " + err.Error())
	}
	defer tcp.Close()
	// open udp
	udp, err := net.ListenPacket("udp", address)
	if err != nil {
		log.Fatalln("udp network error: " + err.Error())
	}
	defer udp.Close()

	go listen(tcp)
	// udp loop
	buff := make([]byte, 1500) // ignore input, reuse buffer
	for {
		_, addr, err := udp.ReadFrom(buff)
		if err != nil {
			log.Println("udp connection error: " + err.Error())
			continue
		}
		q := quote()
		//addr, _ = net.ResolveUDPAddr("udp", "127.0.0.1:2000")
		n, err := udp.WriteTo(q, addr)
		if err != nil || n != len(q) {
			log.Println("udp write error: " + err.Error())
		}
		//log.Println(string(buff) + string(q) + addr.String())
	}
}

func listen(l net.Listener) {
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println("tcp connection error: " + err.Error())
			continue
		}
		q := quote()
		n, err := conn.Write(q)
		if err != nil || n != len(q) {
			log.Println("tcp write error: " + err.Error())
		}
		conn.Close()
	}
}

func quote() []byte {
	n := rand.Intn(len(quotes))
	q := quotes[n]
	// protocol is ascii, but we send whatever is in the file
	return []byte(q)
}

func readquotes(filename string) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalln("error reading quotes: " + err.Error())
	}

	lines := strings.Split(string(bytes), "\n")
	lines = append(lines, "\n") // always end with blank line for last quote
	var q string                // current quote
	for _, line := range lines {
		// leading # is a comment line
		if strings.HasPrefix(line, "#") {
			continue
		}
		// blank lines separate quotes
		if line == "" {
			if q != "" {
				quotes = append(quotes, q)
			}
			q = ""
			continue
		}
		q += line + "\n"
	}

	if len(quotes) == 0 {
		log.Fatalln("error reading quotes; no quotes read from: " + filename)
	}
}
