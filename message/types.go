package main

import (
	"bytes"
	"errors"
	"time"
)

// Message packet information.
// The total length of the message shall be less than 512 octets.
// This includes all eight parts, and any terminating nulls.
type Message struct {
	Revision   byte   // always decimal 66, 'B' for protocol version 2
	RECIPIENT  string // ignored becuse we use desktop notification
	RECIP_TERM string // ignored becuse we use desktop notification
	MESSAGE    string // allow utf8 or whatever the client sends
	// Subsequent parts were not present in version 1 of the
	// Message Send Protocol.
	SENDER      string // The username of the sender; should not be empty.
	SENDER_TERM string // ignored becuse we use desktop notification
	COOKIE      string // cookie & UDP port identify duplicate messages
	SIGNATURE   string
}

func NewMessage(message, sender string) *Message {
	m := &Message{}
	m.Revision = 'B'
	m.MESSAGE = message
	m.SENDER = sender
	m.COOKIE = string(time.Now().UnixNano())
	return m
}

func (m *Message) Bytes() []byte {
	b := &bytes.Buffer{}
	b.WriteByte(m.Revision) // not followed by NULL
	writeCString(b, m.RECIPIENT)
	writeCString(b, m.RECIP_TERM)
	writeCString(b, m.MESSAGE)
	writeCString(b, m.SENDER)
	writeCString(b, m.SENDER_TERM)
	writeCString(b, m.COOKIE)
	writeCString(b, m.SIGNATURE)
	return b.Bytes()
}

func Parse(b []byte) (*Message, error) {
	m := NewMessage("", "")
	if len(b) == 0 || b[0] != 'B' {
		return nil, errors.New("Parse Message: Empty or bad version")
	}
	lines := bytes.Split(b[1:], []byte{byte(0)}) // split on NULL
	if len(lines) != 7 {
		return nil, errors.New("Parse Message: bad format")
	}
	m.RECIPIENT = string(lines[0])
	m.RECIP_TERM = string(lines[1])
	m.MESSAGE = string(lines[2])
	m.SENDER = string(lines[3])
	m.SENDER_TERM = string(lines[4])
	m.COOKIE = string(lines[5])
	m.SIGNATURE = string(lines[6])
	return m, nil
}

// single character indicating positive ("+") or negative ("-") acknowledgment
type ackType = byte

const (
	positiveACK ackType = '+' // the message was successfully delivered
	negativeACK ackType = '-' // the message was NOT delivered
)

type Reply struct {
	ACK     ackType // positive ("+") or negative ("-") acknowledgment
	MESSAGE string  // optional
}

func (r *Reply) Bytes() []byte {
	b := &bytes.Buffer{}
	b.WriteByte(r.ACK)
	writeCString(b, r.MESSAGE)
	return b.Bytes()
}

func writeCString(buff *bytes.Buffer, s string) {
	buff.Write([]byte(s))
	buff.WriteByte(0)
}
