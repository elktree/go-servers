// Message Send Protocol 2
//
// test TCP and UDP:
//
// echo -e 'Brecip\0term\0msg\0sender\0term\0cookie\0sig' | ncat localhost 1818
//
// echo -e 'Brecip\0term\0msg\0sender\0term\0cookie\0sig' | ncat -u localhost 1818
//
package main

import (
	"flag"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	dn "gitlab.com/elktree/go-servers/utils/desktopnotify"
	//dn "../utils/desktopnotify"
)

var port *int = flag.Int("port", 18, "the network port to listen for messages")
var in chan *Message = make(chan *Message, 100)

func main() {
	defer close(in)
	flag.Parse()

	// shutdown on ^C
	sigterm := make(chan os.Signal, 1)
	defer close(sigterm)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)

	go listenTcp()
	go listenUdp()

	// notify loop (blocking)
	for {
		select {
		case m := <-in:
			go Notify(m)
		case <-sigterm:
			os.Exit(0)
		}
	}
}

func Notify(msg *Message) {
	err := dn.Notify(msg.SENDER, msg.MESSAGE)
	if err != nil {
		log.Println("Notify error: " + err.Error())
	}
}

func listenTcp() {
	l, err := net.Listen("tcp", ":"+strconv.Itoa(*port))
	if err != nil {
		panic("Network error: " + err.Error())
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println("Network error: " + err.Error())
			conn.Close()
			continue
		}

		var b []byte = make([]byte, 512) // 512 max packet size
		n, err := io.ReadFull(conn, b)
		b = b[:n] // trim []byte
		//log.Printf("%v", b)
		if err != nil && err != io.ErrUnexpectedEOF {
			log.Println("Network error: " + err.Error())
			conn.Close()
			continue
		}

		msg, err := Parse(b)
		if err != nil {
			log.Println("Message error: " + err.Error())
			conn.Close()
			continue
		}
		in <- msg
	}
}

func listenUdp() {
	conn, err := net.ListenPacket("udp", ":"+strconv.Itoa(*port))
	if err != nil {
		panic("Network error: " + err.Error())
	}
	defer conn.Close()

	for {
		var b []byte = make([]byte, 512) // 512 max packet size
		n, addr, err := conn.ReadFrom(b)
		if err != nil {
			log.Println("Network error: " + err.Error())
			continue
		}
		b = b[:n] // trim []byte
		//log.Printf("%v", b)

		msg, err := Parse(b)
		if err != nil {
			log.Println("Message error: " + err.Error())
			continue
		}
		in <- msg

		// send a reply
		reply := Reply{ACK: positiveACK, MESSAGE: "recieved"}
		_, err = conn.WriteTo(reply.Bytes(), addr)
		if err != nil {
			log.Println("UDP Reply error: " + err.Error())
		}
	}
}
