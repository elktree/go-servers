// desktopnotify is a cheap hack to keep things small.
// If you need this functionallity use beeep:
// https://github.com/gen2brain/beeep
package desktopnotify

import (
	"os/exec"
	"runtime"
)

func Notify(sender, message string) error {
	var cmd *exec.Cmd

	switch runtime.GOOS {
	case "darwin":
		// copied from beeep
		osa, err := exec.LookPath("osascript")
		if err != nil {
			return err
		}
		cmd = exec.Command(osa, "-e", `display notification `+message+` with title `+sender)
	case "windows":
		cmd = exec.Command("msg", "*", "Message from "+sender, "\n\n", message)
	default:
		cmd = exec.Command("notify-send", sender, message)
	}

	return cmd.Run()
}
